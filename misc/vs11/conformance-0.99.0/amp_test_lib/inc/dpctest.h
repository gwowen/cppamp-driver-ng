// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/**********************************************************************************
* dpctest.h
*
* DPCTest library is a set of helper functions for DPC++ tests. 
*
* This is now just a wrapper for amptest.h as dpctest.h has been deprecated.
* Use amptest.h instead.
**********************************************************************************/

#ifndef DPCTEST_H
#define DPCTEST_H

#include <amptest.h>

#endif
